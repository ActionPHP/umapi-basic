<?php
namespace AdobeUMAPI;

require_once "unlock.php";

require_once dirname(__FILE__). '/../adbumapi/adb-api-connect.php';

$start_time = time();

$API = new AdbAPIConnect;

$cron_log = [

	"start_time" => $start_time
];

//We want all the group members from a specific group
$group_name = "CDI Checkout";

//So we count them - or better still get memberCount

$cron_log['groupName'] = $group_name;
$count = 0;


$_group_users = $API->getGroupUsers($group_name);
$_group_users = json_decode($_group_users);
//print_r($_group_users);
if($_group_users && isset($_group_users->users)){
	$group_users = $_group_users->users;

//We want to log them out

foreach ($group_users as $group_user) {
	
	$username = $group_user->username;

	$API->removeUserFromGroup($username, $group_name);
	sleep(13);//We will wait for 13 seconds to ensure we don't exceed the 5 requests per minute limit.

	$count++;
}
}

if($_group_users && isset($_group_users->error_code)){

	$cron_log['error_code'] = $_group_users->error_code;
	$cron_log['message'] = $_group_users->message;
}

$cron_log['count'] = $count;

//$cron_log["group_users"] = $group_users;

$end_time = time();

$cron_log['end_time'] = $end_time;

//Now, we will log all this information.



//We will now write this into the file
$cron_log_file = fopen('cron_log.txt', "w");

//Let's turn our `$cron_log` into a string

$cron_log = json_encode($cron_log);
$file = fwrite($cron_log_file, $cron_log);

fclose($cron_log_file);
die($cron_log . "\n\n");



