<?php

if(isset($_GET['key'])){

	$key = trim($_GET['key']);

} elseif(null != getopt(null, ["key:"])){

	$opt = getopt(null,["key:"]);
	$key = $opt['key'];

} else {
	$key = null;
}

include_once "cron.config.php";

if ($cron_key != $key){

	$error = "That's not the key to the kingdom!";

	$_error = ["error" => $error];

	$_error = json_encode($_error);
	die($_error . "\n");
}