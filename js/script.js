$(document).ready(function(){

	$("body").on("keyup", "#username", function(e){
e.stopPropagation();
		if(e.which == 13){
			$("body").find("#start-check-in-button").click();
		}
		return false;
	});

	$("body").on("click", "#start-check-in-button", function(e){
		doCheckIn(e);
	});

	var doCheckIn = function(e){
		
		showError = function(error) {

			$("#error-message").text(error);
			$("#error-box").show();
			
		}

		hideError = function(){
			$("#error-box").hide();
		}

		showLoader = function($el){

			$el.html('Please wait... <span class="loader"></span>');

		}
		//We will use this to manage the page refresh timer
		var _refresh;
		var _timeout_length = 60000; //Timeout length in milliseconds
		
		//We don't need to keep looking at the error message
		hideError();
		var username = $("#username").val();

		$button = $(e.currentTarget);
		showLoader($button);

		$.post(
				"check-in/",
				{
					username: username
				},

				function(_data) {
					
					var data = JSON.parse(_data);

					if(data.result == "success"){

						
						var product_groups = data.product_groups;

						var _button;


						$(product_groups).each(function(index, group){

							if(data.license_quota_full){

								_button = "<b>License quota full.</b>";

							} else if(group.user_checked_in){

							_button = '<button type="button" data-group-name="'+ group.groupName +'" class="warning button check-out-button" >Check out</button>';
							
							} else {

								_button = '<button type="button" data-group-name="'+ group.groupName +'" class="success button check-in-button">Check in</button>'
							}
							var el = '<tr><td>'+ group.productName +'</td><td >'+ _button +'</td></tr>';

							$("#check-in-table").append(el);

						});
						$("#login-form").hide();
						$("#check-in-screen").show();

						_refresh = setTimeout(function(){
							location.reload(true);
						}, _timeout_length);



					} else {

						if(data.message){

							error = data.message;

						} else {

							error = "Something is wrong...";

						}

						showError(error);

						$button.text("Continue...");
					}

					$("body").on("click", ".check-in-button", function(e){
						

						e.stopPropagation();
						
						//Let's give the timer another minute

						clearTimeout(_refresh);//restart the timeout
						_refresh = setTimeout(function(){
							location.reload(true);
						}, _timeout_length);
						var $button = $(e.currentTarget)
						var group_name = $button.data("group-name");

						showLoader($button);
						$.post(
								"check-in/group/",
								{
									"action" : "add",
									"group_name" : group_name
								},

								function(_data){

									var data = JSON.parse(_data);

									if(data.result == "success"){

										$button.removeClass("check-in-button success");
										$button.addClass("check-out-button warning");
										$button.text("Check out");

										$("#finish-button").show();
									} else {
										alert("Something went wrong. Please try again.");
										location.reload(true);

									}

								}
							);

					});

					$("body").on("click", ".check-out-button", function(e){
						
						e.stopPropagation();

						clearTimeout(_refresh);//restart the timeout
						_refresh = setTimeout(function(){
							location.reload(true);
						}, _timeout_length);


						var $button = $(e.currentTarget)
						var group_name = $button.data("group-name");
						showLoader($button);
						$.post(
								"check-in/group/",
								{
									"action" : "remove",
									"group_name" : group_name
								},

								function(_data){

									var data = JSON.parse(_data);

									if(data.result == "success"){

										$button.removeClass("check-out-button warning");
										$button.addClass("check-in-button success");
										$button.text("Check in");
										
										$("#finish-button").show();

									}
								}
							);
						

					});
				}

			);

	}
});