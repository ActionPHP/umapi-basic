<?php
session_start();

use AdobeUMAPI\AdbAPIConnect as API;

include_once "../includes/bootstrap.php";

if(isset($_POST['username'])){
	
	$username = trim(stripslashes($_POST["username"]));

} else {
	die("Sh...");
}

$API = new API;

//$users = $API->users();print_r($users);
$_user = $API->getUser($username);

$_user = json_decode($_user);

if(!$_user){

	$data = [

		"result" => "error",
		"message" => "User not found $username"
	];


	echo json_encode($data); die();

}

//If the user is NOT FOUND let's send that error!
if(is_object($_user) ){

	if(isset($_user->result) && $_user->result != "success"){
		//Let's turn our user info back to JSON
		echo json_encode($_user); die();
	}

	if(isset($_user->error_code)){

		echo json_encode($_user); die();

	}
}
//If user is found, let's get the groups the user is in.
$user = $_user->user;


$_SESSION['adbumapi_username'] = $user->username;

if(property_exists($user, 'groups')){

	$user_groups = $user->groups;

} else {
	$user_groups = [];
}

//Now we will get all the available product groups

$org_groups = $API->getGroups();

$product_groups = [];

$group_name = "CDI Checkout"; //We're only working with one group at the moment

foreach ($org_groups as $group) {
	
	if($group->type == "PRODUCT_PROFILE" && $group->groupName == $group_name){
		$product_groups[] = $group;
	}

}

//Then, we will check what groups the user is already in

foreach ($product_groups as $product_group) {
	
	foreach ($user_groups as $user_group) {
		
		if(in_array($product_group->groupName, $user_groups)){

			$product_group->user_checked_in = true;

		}

	}

}

//Let's check the license quota
//@warning - this only works if we're working with only one group - "CDI Checkout". If the user is in the group, we can skip and just let them checkout.
$license_quota_full = false;
if(!isset($product_group->user_checked_in) || !$product_group->user_checked_in){
	
	//OK, we have to now see if there are available licenses
	//@warning We're assuming that we're only dealing with only one group at the moment - "CDI Checkout". This script will have to be modified if we're working with more than one group. So, we're assuming the "CDI Checkout" is the only group in $product_groups.
    $group = $product_groups[0];
	$license_quota = $group->licenseQuota;

	//We need all the members inside the group and count them.

	$group_members = $API->getGroupUsers($group_name);
	$group_members = json_decode($group_members);


	$member_count = $group->memberCount;

	if($member_count >= $license_quota){

		$license_quota_full = true;

	}

}
//
//print_r(get_defined_vars()); die();




//Let's send that data!

$data = [
	"result" => "success",
	"user" => $user,
	"product_groups" => $product_groups,
	"license_quota_full" => $license_quota_full
];

$data = json_encode($data);
echo $data;
die();