<?php
session_start();
use AdobeUMAPI\AdbAPIConnect as API;

include_once dirname(__FILE__)."/../../includes/bootstrap.php";

$data  = json_decode(json_encode($_POST));
if(!$data){
	die("Shh...");
}
$action = $data->action;

$group_name = $data->group_name;

$username = $_SESSION["adbumapi_username"];

$API = new API;

switch ($action) {
	case 'add':
		$result = $API->addUserToGroup($username, $group_name);
		break;
	case 'remove':
		$result = $API->removeUserFromGroup($username, $group_name);
		break;
	
	default:
		# code...
		break;
}



print_r($result);die();
