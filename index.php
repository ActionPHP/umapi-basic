<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome!</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">

  </head>
  <body>
    <div class="grid-container">


      <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
          <div id="" class="callout">
             <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
          <h1>Welcome!</h1>
        </div>
      </div>
           
          <hr />

          <div id="login-form" >
          <h5>Please input your username below:</h5>
          <div id="error-box" class="callout alert" >
			  <h5 id="error-message">This is an alert callout</h5>
			 
			</div>
            <div class="grid-x grid-padding-x">
              <div class="large-12 cell">
                <label>Your username:</label>
                <input type="text" id="username" placeholder="Your organization username..." />
              </div>
            </div>
        
           <div><a href="#" class="button" id="start-check-in-button">Continue...</a></div>
      	</div>

      	<div id="check-in-screen" style="display: none;" >

      		<h5>Please select a product to check in or check out.</h5>

      		<table  id="check-in-table">
  <thead>
    <tr>
      <th >Product name</th>
      <th >Check in/Check out</th>
      
    </tr>
  </thead>
  <tbody>
    
  </tbody>
</table>
<div><center><a href="" id="finish-button" class="button" >Click here to finish...</a></center></div>

      	</div>


        </div>

       
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

  </body>
</html>
