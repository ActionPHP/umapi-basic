<?php
namespace AdobeUMAPI;

	class AdbAPIConnect {

		private $page = 0;

		private $host;
		private $endpoint;
		private $org_id;
		private $url = 'https://ims-na1.adobelogin.com/ims/exchange/jwt';
		private $username;
		private $client_secret;
		private $api_key;

		private $access_token ;

		public function __construct()
		{
			include dirname(__FILE__)."/../vendor/JWT.php";
			include dirname(__FILE__)."/../config/config.php";

			
			$this->api_key = $api_key;

			$this->host = $host;
			$this->endpoint = $endpoint;
			$this->org_id = $org_id;
			$this->client_secret = $client_secret;

			$expire = time() + 60*60;
			$token = array(
			    "exp" => $expire,
			    "iss" => $org_id,
			    "aud" => "https://" . $ims_host . "/c/" . $api_key,
			    "sub" => $tech_acct,
			    "https://" . $ims_host . '/s/' . 'ent_user_sdk' => true

			);

			$jwt = \JWT::encode($token, $privateKey, 'RS256');

			//Let's get access token

			$access_token = $this->access_token($jwt);
		}

		private function access_token($jwt)
		{	

			$_post_data = [

				"client_id" => $this->api_key,
				"client_secret" => $this->client_secret,
				"jwt_token" => $jwt

			];

			$post_data =http_build_query($_post_data);
			$_access_token = $this->connect("POST", $post_data, true);
			
			$_access_token = json_decode($_access_token);

			$access_token = $_access_token->access_token;
			$this->access_token = $access_token;
			return $access_token;
		}

		private function headers() {

			return [
			    "Content-type: application/json",
			    "Accept: application/json",
			    "x-api-key: "  . $this->api_key,
			    "Authorization: Bearer " . $this->access_token 
			];

		}
		/*
		Here we will construct the request
		*/
		private function request($value='')
		{
			# code...
		}

		public function url()
		{	//@todo In the full up, break down the url with conditionals so it can take a larger variety of resources
			$url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id . "/" . $this->page ;

			return $url;
		}

		public function addUserToGroup($username, $group_name)
		{	



		$_post_data = [[

			"user" => $username,
			"do" => [

				[

				"add" => [

						"group" => [$group_name]

				]

			]]

		]];

			$post_data = json_encode($_post_data);

			//$this->url = 'https://usermanagement.adobe.io/v2/usermanagement/action/{orgId}';

			$this->resource = "action";

			$url = $url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id;

			$this->url = $url;

			$result = $this->connect("POST", $post_data);

			return $result;
		}

		public function removeUserFromGroup($username, $group_name)
		{	


		$_post_data = [[

			"user" => $username,
			"do" => [

				[

				
				"remove" => [

						"group" => [$group_name]

				]

			]]

		]];

			$post_data = json_encode($_post_data);

			$this->resource = "action";

			$url = $url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id;

			$this->url = $url;

			$result = $this->connect("POST", $post_data);
			return $result;
		}

		private function connect($method='GET',$post_data=[], $jwt=false)
		{	
			$request_headers =  $this->headers();
			
			$url = $this->url;

			$curl = curl_init();
			
			curl_setopt($curl, CURLOPT_URL, $url);
			if(!$jwt){
					curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
				}

			if("POST" == $method){

				curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);

			}

			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			
			$result = curl_exec($curl);
					
			
			return $result;
		}

		/**
		 * Let's get all the users! Yey!
		 */
		public function users()
		{	return;
			//Ok so, we will connect to the API and get the first page of the result

			$this->page = 0;

			$this->resource = "users";

			$done = false;
			$users = [];
$url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id . "/" . $this->page ;
$this->url = $url;

				/*$_users = $this->connect();

				return $_users;
*/
			while (!$done) {
				//echo "</pre><pre>";
				$_users = $this->connect();
				$_users = json_decode($_users);
				//print_r($_users->users);

				if(isset($_users->users) && is_array($_users->users)){
					$users = array_merge($users, $_users->users);
				}
			//	echo "</pre><pre>";
//print_r($_users);

				if(isset($_users->error_code)){
					continue;
				}
				sleep(5);
				$last_page = $_users->lastPage;

				if($last_page){
					$done = true;
				}
				
				$this->page += 1;

				if($this->page > 0 && !$users){
					continue;
					return "Error!";
				}
			}

			return $users;
			
		}

		public function getGroups()
		{
			
			$this->page = 0;

			$this->resource = "groups";

			$url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id . "/" . $this->page ;

			$this->url = $url;

			//OK, let's get these groups
			$done = false;
			$groups = [];

			//@todo - In full version, let's inspect the headers for page count,etc.
			while (!$done) {
				
				$_groups = $this->connect();
				$_groups = json_decode($_groups);

				$groups = array_merge($groups, $_groups->groups);
				

				$last_page = $_groups->lastPage;

				if($last_page){
					$done = true;
				}
				
				$this->page += 1;
			}

			return $groups;
		}

		public function createUser($value='')
		{	
			$this->resource = 'action';
			$this->page = '';
			$result = $this->connect();

			
			$url = $this->url();
			print_r('https://usermanagement.adobe.io/v2/usermanagement/action/77B37B2A53AC14FC0A490D45@AdobeOrg' . "\n");
			print_r($url . "\n\n");
			print_r($result);
		}


		public function getUser($username)
		{	

			$this->resource = "organizations";
			$this->username = $username;

			$url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id . "/users/" . $this->username;

			$this->url = $url;

			$user = $this->connect();
			return $user;
		}

		public function getGroupUsers($group_name)
		{	
			$this->page = 0;
			$group_name = rawurlencode($group_name);
			$this->resource = "users";
			$this->group_name = $group_name;

			$url = "https://" . $this->host . $this->endpoint . "/" . $this->resource . "/" . $this->org_id . '/' . $this->page. '/' . $this->group_name ;

			$this->url = $url;

			$group_users = $this->connect();

			//print_r(get_defined_vars()); die();
			return $group_users;
		}
	}